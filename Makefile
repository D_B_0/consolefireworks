CXX      = g++
CXXFLAGS = -pedantic-errors -Wall -Wextra -Werror -MMD
LDFLAGS  =
BUILD    = bin
OBJ_DIR  = $(BUILD)/obj
APP_DIR  = $(BUILD)
TARGET   = console_fireworks
SRC      = $(wildcard src/*.cpp)
INCLUDE  =
HEADERS  = $(wildcard src/*.hpp)
OBJECTS  = $(SRC:%.cpp=$(OBJ_DIR)/%.o)

.PHONY: all build run clean info

all: build $(APP_DIR)/$(TARGET)

$(OBJ_DIR)/%.o: %.cpp $(HEADERS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@

$(APP_DIR)/$(TARGET): $(OBJECTS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -o $(APP_DIR)/$(TARGET) $^ $(LDFLAGS)

build:
	@echo $(CXXFLAGS) $(LDFLAGS) $(INCLUDE) | sed "s/ /\n/g" > compile_flags.txt
	@mkdir -p $(APP_DIR)
	@mkdir -p $(OBJ_DIR)

run: all
	$(APP_DIR)/$(TARGET)

clean:
	-@rm -rvf $(OBJ_DIR)/*
	-@rm -rvf $(APP_DIR)/*

info:
	@echo "[*] Application dir: ${APP_DIR}"
	@echo "[*] Object dir:      ${OBJ_DIR}"
	@echo "[*] Sources:         ${SRC}    "
	@echo "[*] Objects:         ${OBJECTS}"