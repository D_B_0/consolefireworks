#include "Canvas.hpp"

#include <chrono>
#include <iostream>
#include <thread>

static constexpr uint32_t WIDTH = 190;
static constexpr uint32_t HEIGHT = 45;
static constexpr uint32_t FPS = 60;
static constexpr double DT = 1.0 / FPS;
static constexpr uint32_t DT_MS = static_cast<uint32_t>(
    1000.0 / FPS + .5); // Not the best rounding, but constexpr

int main()
{
  Canvas c(WIDTH, HEIGHT);
  c.Fill('.');
  c.PrintCanvas();
  bool running = true;
  int w = 6;
  int h = 3;
  double dx = 50;
  double dy = 0;
  double ddx = 0;
  double ddy = 100;
  double x = WIDTH / 2.0 - w / 2.0;
  double y = HEIGHT / 3.0;
  while (running)
  {
    // FrameRate
    std::this_thread::sleep_for(std::chrono::milliseconds(DT_MS));
    // Clear canvas
    c.Fill('.');

    // Draw and Display
    c.DrawRect(x, y, w, h, '#');

    c.ResetCursor();
    c.PrintCanvas();

    // Update
    dx += ddx * DT;
    dy += ddy * DT;
    x += dx * DT;
    y += dy * DT;
    if (x >= static_cast<int>(WIDTH) - w)
    {
      x = static_cast<int>(WIDTH) - w;
      dx *= -1;
    }
    if (x <= 0)
    {
      x = 0;
      dx *= -1;
    }
    if (y >= static_cast<int>(HEIGHT) - h)
    {
      y = static_cast<int>(HEIGHT) - h;
      dy *= -1;
    }
    if (y <= 0)
    {
      y = 0;
      dy *= -1;
    }
  }
}