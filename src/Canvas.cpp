#include "Canvas.hpp"

Canvas::Canvas(uint64_t width, uint64_t height)
    : m_width(width), m_height(height)
{
  m_canvas = new char[m_width * m_height];
}

Canvas::~Canvas()
{
  delete[] m_canvas;
}

void Canvas::ResetCursor()
{
  for (size_t i = 0; i < m_height; i++)
    std::cout << "\033[A\r" << std::flush;
}

void Canvas::PrintCanvas()
{
  for (size_t i = 0; i < m_width * m_height; i++)
  {
    std::cout << m_canvas[i];
    if (i % m_width == m_width - 1)
      std::cout << "\n";
  }
}

void Canvas::DrawPoint(uint64_t x, uint64_t y, const char point)
{
  if (x > m_width - 1 || y > m_height - 1)
    assertm(false, "Tried to draw outside of bounds");
  m_canvas[x + y * m_width] = point;
}

void Canvas::DrawRect(int x, int y, uint64_t width, uint64_t height,
                      const char point)
{
  int w = width;
  int h = height;
  for (int dx = 0; dx < w; dx++)
  {
    for (int dy = 0; dy < h; dy++)
    {
      if (x + dx <= static_cast<int>(m_width) - 1 && x + dx >= 0
          && y + dy <= static_cast<int>(m_height) - 1 && y + dy >= 0)
        DrawPoint(x + dx, y + dy, point);
    }
  }
}

void Canvas::Fill(const char point)
{
  for (size_t i = 0; i < m_width * m_height; i++)
    m_canvas[i] = point;
}
