#pragma once
#include <cassert>
#include <cstdint>
#include <iostream>

#define assertm(exp, msg) assert(((void)msg, exp))

class Canvas
{
public:
  Canvas(uint64_t width, uint64_t height);
  ~Canvas();

  void ResetCursor();
  void PrintCanvas();
  void DrawPoint(uint64_t x, uint64_t y, const char point = '#');
  void DrawRect(int x, int y, uint64_t width, uint64_t height,
                const char point = '#');
  void Fill(const char point);

private:
  uint64_t m_width;
  uint64_t m_height;
  char *m_canvas;
};